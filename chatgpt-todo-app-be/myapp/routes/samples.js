var express = require("express");
var router = express.Router();

router.get("/", function (req, res, next) {
  var param = { name: "sample", age: "21" };
  res.header("Content-Type", "application/json; charset=utf-8");
  res.send(param);
});

router.get("/hello", function (req, res, next) {
  var param = { name: "sample", age: "20" };
  res.header("Content-Type", "application/json; charset=utf-8");
  res.send(param);
});

module.exports = router;
