var express = require("express");
var router = express.Router();
require("dotenv").config();

router.post("/:id", function (req, res, next) {
  res.set({ "Access-Control-Allow-Origin": "*" });
  res.header("Content-Type", "application/json; charset=utf-8");

  const dbInfo = {
    host: process.env.DB_HOST || process.env.HEROKU_DB_HOST,
    user: process.env.DB_USER || process.env.HEROKU_DB_USER,
    password: process.env.DB_PASS || process.env.HEROKU_DB_PASS,
  };
  const mysql = require("mysql");
  const pool = mysql.createPool(dbInfo);
  pool.query(
    "update todoappdb.todos set completedDateTime = ? where id = ?",
    [new Date(), req.params.id],
    function (err, result) {
      if (err) {
        console.log("エラー発生" + err);
        res.status(500).send({ error: err.message });
        return;
      }
      console.log(result);
      res.json(result);
    }
  );
});

module.exports = router;
