require("dotenv").config();
const cron = require("node-cron");
const moment = require("moment");
const API_KEY = process.env.API_KEY || process.env.HEROKU_API_KEY;
const { Configuration, OpenAIApi } = require("openai");
const configuration = new Configuration({
  apiKey: API_KEY,
});
const openai = new OpenAIApi(configuration);

// ChatGPT APIを使用してメッセージの応答を取得する関数
async function getChatResponse(message) {
  try {
    const completion = await openai.createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: [{ role: "user", content: message }],
    });
    return completion.data.choices[0].message.content;
  } catch (err) {
    console.error("OpenAI APIでエラーが発生しました: " + err);
  }
}

const dbInfo = {
  host: process.env.DB_HOST || process.env.HEROKU_DB_HOST,
  user: process.env.DB_USER || process.env.HEROKU_DB_USER,
  password: process.env.DB_PASS || process.env.HEROKU_DB_PASS,
};
const mysql = require("mysql");
const pool = mysql.createPool(dbInfo);

module.exports = function (wss) {
  wss.on("connection", function connection(ws) {
    console.log("connected websocket");

    cron.schedule("* * * * *", async () => {
      pool.query(
        "SELECT * FROM todoappdb.todos where completedDateTime IS NULL",
        async function (err, result) {
          if (err) {
            console.log("エラー発生" + err);
            ws.status(500).send({ error: err.message });
            return;
          }
          let throughDeadLineFlg = false;
          console.log("deadLineをチェックします。");
          for (let todo of result) {
            let currentDateTime = moment().format("YYYY/MM/DD HH:mm:ss");
            const deadLine = new Date(todo.deadLine);
            let formattedDeadline = deadLine.toLocaleString("ja-JP", {
              year: "numeric",
              month: "2-digit",
              day: "2-digit",
              hour: "2-digit",
              minute: "2-digit",
              second: "2-digit",
            });
            currentDateTime = new Date(currentDateTime);
            formattedDeadline = new Date(formattedDeadline);

            // 現在の年月日を取得する
            const currentDate = currentDateTime.getDate();
            const currentMonth = currentDateTime.getMonth() + 1;
            const currentYear = currentDateTime.getFullYear();

            // 過去に通知した年月日を取得する
            const notifiedDate = todo.notifiedDateTime
              ? todo.notifiedDateTime.getDate()
              : null;
            const notifiedMonth = todo.notifiedDateTime
              ? todo.notifiedDateTime.getMonth() + 1
              : null;
            const notifiedYear = todo.notifiedDateTime
              ? todo.notifiedDateTime.getFullYear()
              : null;

            // 今日の通知済みかどうかを判定する
            let notifiedFlg = false;
            if (
              currentDate === notifiedDate &&
              currentMonth === notifiedMonth &&
              currentYear === notifiedYear
            ) {
              notifiedFlg = true;
            }

            // 今日の通知済みでない場合は、通知する
            if (currentDateTime >= formattedDeadline && !notifiedFlg) {
              console.log("期限が来たやつがいるよ");
              const response = await getChatResponse(
                "もし「" +
                  todo.todoContent +
                  "」というタスクをすぐに行わないと、どんな大変な事態が起こるかを恐怖感を煽る形で説明してください。「" +
                  todo.todoContent +
                  "」をする理由は「" +
                  todo.reason +
                  "」です。60字以下のメッセージと、さらに詳しく200字以下で説明したメッセージの2文にわけて返信してください。下記のテンプレートに合わせてください。メッセージ1: ここに60字程度のメッセージメッセージ2:ここに詳しく説明したメッセージ"
              );
              pool.query(
                "update todoappdb.todos set notifiedDateTime = ? where id = ?",
                [new Date(), todo.id],
                function (err, result) {
                  if (err) {
                    console.log("エラー発生" + err);
                    ws.status(500).send({ error: err.message });
                    return;
                  }
                }
              );
              ws.send(JSON.stringify({ flg: 1, message: response }));
            } else {
              throughDeadLineFlg = true;
            }
          }
          if (throughDeadLineFlg) {
            ws.send(
              JSON.stringify({ flg: 0, message: "期限が来たやつはいないよ" })
            );
          }
        }
      );
    });
  });
};
