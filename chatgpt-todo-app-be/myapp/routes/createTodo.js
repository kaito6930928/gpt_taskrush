var express = require("express");
var router = express.Router();
require("dotenv").config();
var { v4: uuidv4 } = require("uuid");

router.post("/", function (req, res, next) {
  res.set({ "Access-Control-Allow-Origin": "*" });
  //   リクエストボディからTODOを作成する
  const todo = {
    id: uuidv4(),
    todoContent: req.body.todoContent,
    deadLine: req.body.deadLine,
    reason: req.body.reason,
    benefit: req.body.benefit,
    createdDateTime: new Date(),
  };
  res.header("Content-Type", "application/json; charset=utf-8");

  //   データベースにTODOを追加する処理\
  const dbInfo = {
    host: process.env.DB_HOST || process.env.HEROKU_DB_HOST,
    user: process.env.DB_USER || process.env.HEROKU_DB_USER,
    password: process.env.DB_PASS || process.env.HEROKU_DB_PASS,
  };
  const mysql = require("mysql");
  const pool = mysql.createPool(dbInfo);
  pool.query("insert into todoappdb.todos set ?", todo, function (err, result) {
    if (err) {
      console.log("エラー発生" + err);
      res.status(500).send({ error: err.message });
      return;
    }
    console.log(result);
  });
  res.json(todo);
});

module.exports = router;
