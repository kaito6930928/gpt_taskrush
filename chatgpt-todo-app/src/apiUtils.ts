export async function callExpressAPI(
  url: string,
  method: string,
  bodyParams?: any
) {
  try {
    const requestOptions: RequestInit = {
      method: method,
      headers: {
        "Content-Type": "application/json",
      },
      mode: "cors",
    };

    if (method !== "GET") {
      requestOptions.body = JSON.stringify(bodyParams);
    }

    const response = await fetch(
      "http://localhost:" +
        (process.env.REACT_APP_PORT || process.env.HEROKU_PORT) +
        "/" +
        url,
      requestOptions
    );
    const data = await response.json();
    console.log(data);
    return data;
  } catch (error) {
    console.error("API呼び出しエラー:", error);
  }
}
