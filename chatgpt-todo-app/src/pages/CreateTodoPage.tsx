import React from "react";
import Modal from "react-modal";
import { useForm } from "react-hook-form";
import { Box, Button, TextField, Typography } from "@mui/material";
import { styled } from "@mui/system";

interface CreateTodoPageProps {
  editModalIsOpen: boolean;
  setIsOpen: (isOpen: boolean) => void;
  addTodo: (newTodo: any) => void;
}

// Custom styling
const CustomModal = styled(Modal)(({ theme }) => ({
  backgroundColor: "#f5f5f5", // Modal background color
  color: "black", // Modal text color
  padding: theme.spacing(2),
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "80vw",
  maxWidth: "500px",
  boxShadow: "0 3px 5px 2px rgba(0, 0, 0, .3)",
}));

const CreateTodoPage = (props: CreateTodoPageProps) => {
  async function callExpressAPI(bodyParams: any) {
    try {
      const response = await fetch("http://localhost:3001/createTodo", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(bodyParams),
        mode: "cors",
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.error("API呼び出しエラー:", error);
    }
  }

  Modal.setAppElement("#root");
  function handleCreateTodo() {
    reset();
    props.setIsOpen(false);
  }
  const onSubmit = (data: any) => {
    handleCreateTodo();
    props.addTodo(data);
    callExpressAPI(data);
  };
  const { register, handleSubmit, reset } = useForm();

  const currentDateTime = new Date();
  currentDateTime.setHours(currentDateTime.getHours() + 9);

  return (
    <>
      <CustomModal isOpen={props.editModalIsOpen} sx={{ shadows: 10 }}>
        <Typography variant="h4" component="div">
          Create Todo
        </Typography>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Box marginY={2}>
            <TextField
              id="todoContent"
              label="What to do?"
              fullWidth
              {...register("todoContent")}
            ></TextField>
          </Box>
          <Box marginY={2}>
            <TextField
              id="deadLine"
              defaultValue={currentDateTime.toISOString().substring(0, 16)}
              label="Deadline"
              type="datetime-local"
              fullWidth
              {...register("deadLine")}
            ></TextField>
          </Box>
          <Box marginY={2}>
            <TextField
              id="reason"
              label="Why you do it?"
              fullWidth
              {...register("reason")}
            ></TextField>
          </Box>
          <Box marginY={2}>
            <TextField
              id="benefit"
              label="どんな恩恵が得られる?"
              fullWidth
              {...register("benefit")}
            ></TextField>
          </Box>
          <Button variant="contained" type="submit">
            Create Todo
          </Button>
          <Button
            sx={{ marginLeft: 35 }}
            variant="contained"
            type="button"
            onClick={() => props.setIsOpen(false)}
          >
            close
          </Button>
        </form>
      </CustomModal>
    </>
  );
};

export default CreateTodoPage;
