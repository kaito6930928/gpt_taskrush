import React, { useEffect } from "react";
import CreateTodoPage from "./CreateTodoPage";
import { callExpressAPI } from "../apiUtils";
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography,
} from "@mui/material";
import { styled } from "@mui/system";

interface todo {
  id: string;
  todoContent: string;
  deadLine: string | Date;
  reason: string;
  benefit: string;
}

async function completeTodo(id: string) {
  try {
    const response = await callExpressAPI("completeTodo/" + id, "POST", {
      id: id,
    });
    const data = await response;
    return data;
  } catch (error) {
    console.error("API呼び出しエラー:", error);
  }
}

const Header = styled(Box)(({ theme }) => ({
  backgroundColor: "#3f51b5", // Header background color
  color: "white", // Header text color
  padding: theme.spacing(2),
}));

const Footer = styled(Box)(({ theme }) => ({
  backgroundColor: "#3f51b5", // Footer background color
  color: "white", // Footer text color
  padding: theme.spacing(2),
}));

const MainArea = styled(Box)(({ theme }) => ({
  backgroundColor: "#f5f5f5", // Main background color
  color: "black", // Main text color
  padding: theme.spacing(2),
  flexGrow: 1, // Add this line to fill the remaining space
}));

const Task = styled(Box)(({ theme }) => ({
  backgroundColor: "#e0e0e0", // Task background color
  color: "black", // Task text color
  padding: theme.spacing(2),
  marginBottom: theme.spacing(2),
  display: "flex", // Add this line to make task elements line up in a row
  justifyContent: "space-between", // Add this line to add space between elements
}));

const TodoListPage = () => {
  // ダイアログの表示を制御する新たなステート
  const [dialogOpen, setDialogOpen] = React.useState(false);
  const [dialogTitle, setDialogTitle] = React.useState("");
  const [dialogMessage, setDialogMessage] = React.useState("");
  const [modalIsOpen, setIsOpen] = React.useState(false);
  // todoListをstateとして管理
  const [todoList, setTodoList] = React.useState<Array<todo>>([]);
  const handleOpenModal = () => {
    setIsOpen(true);
  };
  function showNotification(title: string, options: any) {
    // ブラウザが通知をサポートしているか確認する
    if (!("Notification" in window)) {
      console.log("このブラウザはデスクトップ通知をサポートしていません");
      return;
    }
    const notification = new Notification(title, options);
    if (!dialogOpen) {
      setDialogTitle(title);
      setDialogMessage(options.body);
      setDialogOpen(true);
    }
    // 通知をクリックしたときにダイアログを表示する
    notification.onclick = function () {
      window.focus();
      setDialogTitle(title);
      setDialogMessage(options.body);
      setDialogOpen(true);
    };
  }

  //todoListが更新されたらレンダリングする
  //TODOこれマジでいるんか？
  useEffect(() => {}, [todoList]);

  // todoListに新しいTodoを追加する
  const addTodo = (newTodo: any) => {
    setTodoList([...todoList, newTodo]);
  };

  // todoを完了状態にする
  const handleComplete = (id: string) => {
    completeTodo(id);
    const newTodoList = [...todoList];
    const index = newTodoList.findIndex((todo) => todo.id === id);
    newTodoList.splice(index, 1);
    setTodoList(newTodoList);
  };

  // 改行で分割してメッセージを抽出し、"メッセージ1: "および"メッセージ2: "を削除して返す
  function splitMessages(str: string) {
    let splitStr = str.split("\n");
    let messages = {
      message1: splitStr[0].replace("メッセージ1: ", ""),
      message2: splitStr[2].replace("メッセージ2: ", ""),
    };
    return messages;
  }

  const checkDeadLine = () => {
    //TODO WebSocketの接続を閉じる必要あり。現段階ではページは1つしかないのでとりあえず放置
    const socket = new WebSocket("ws://localhost:3001");
    socket.addEventListener("message", function (event) {
      console.log("Message from server ", event.data);
      const websocketResponse = JSON.parse(event.data);
      if (websocketResponse.flg !== 0) {
        const messages = splitMessages(websocketResponse.message);
        const title = messages.message1;
        const message = messages.message2;
        const notificationOptions = {
          body: message,
        };
        showNotification(title, notificationOptions);
      }
    });
    return socket;
  };

  useEffect(() => {
    //TODOなぜかここで定義しないとエラーになる
    const fetchTodoList = async () => {
      try {
        const response = await callExpressAPI("getTodoList", "GET", null);
        const data = await response;
        data.forEach((todo: { deadLine: string | Date }) => {
          const deadLine = new Date(todo.deadLine);
          todo.deadLine = deadLine.toLocaleString("ja-JP", {
            year: "numeric",
            month: "2-digit",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit",
            second: "2-digit",
          });
        });
        setTodoList(data);
      } catch (error) {
        console.error("API呼び出しエラー:", error);
      }
    };

    fetchTodoList();
    const socket = checkDeadLine();
    return () => socket.close();
  }, []);

  return (
    <Box sx={{ display: "flex", flexDirection: "column", minHeight: "100vh" }}>
      <Header>
        <Typography variant="h4" component="div">
          TodoListPage
        </Typography>
      </Header>
      <MainArea>
        {todoList?.map((todo) => (
          <Task key={todo.id}>
            {todo.todoContent} deadLine:{todo.deadLine as string}
            <Button variant="contained" onClick={() => handleComplete(todo.id)}>
              完了
            </Button>
          </Task>
        ))}
        <Button variant="contained" onClick={handleOpenModal}>
          Create Todo
        </Button>
      </MainArea>
      <Footer>
        <Typography variant="body1" component="div">
          {todoList.length} tasks remaining
        </Typography>
      </Footer>
      <CreateTodoPage
        editModalIsOpen={modalIsOpen}
        setIsOpen={setIsOpen}
        addTodo={addTodo}
      />
      {dialogOpen && (
        <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)}>
          <DialogTitle>{dialogTitle}</DialogTitle>
          <DialogContent>
            <DialogContentText>{dialogMessage}</DialogContentText>
          </DialogContent>
          <Button onClick={() => setDialogOpen(false)}>閉じる</Button>
        </Dialog>
      )}
    </Box>
  );
};

export default TodoListPage;
